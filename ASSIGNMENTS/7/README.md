# Assignment 7
## Due April 10<sup>th</sup> before class.

Write a responsive todo app with authentication implemented. 

## Inspiration
From the class:
- `simpleAuth`
- `rss+Users`
- `todoEg`

## Submission
Demo in class.

## Grading
1. 5 pts : if app hosted on AWS.
2. 5 pts: materialize theming + css.
3. 5 pts: registration/login works
4. 5 pts: todo functionality
5. 2 pts(extra credit): if todos can be reordered. (hint: use https://github.com/dbushell/Nestable).
